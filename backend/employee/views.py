from django.shortcuts import render
from django.views import View
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404, response
from django.http.response import JsonResponse
from restAPI.models import *
from restAPI.serializers import *


class Employees(APIView):

    def get(self, request):
        employees = Employee.objects.all()
        serializer = EmployeeSerializer(employees, many=True)
        return Response(serializer.data, status = status.HTTP_200_OK)

    def post(self, request):
        serializer = EmployeeSerializer(data= request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class OrderByCustomer(APIView):

    def get_by_name(self, request):
        print(request)
        # Get the customer id from the customers name
        customer = Customer.objects.get(Name= request['Name'], Email=request['Email'])
        serialized = CustomerSerializer(customer)
        return serialized.data.get('id')
        # Use the customer id the get the orders by customer

        # Return the orders with the customers name in a custom dictionary
    def post(self, request):
        customer_id = self.get_by_name(request.data)
        customer_orders = Item.objects.filter(Customer = customer_id)
        serialized = ItemSerializer(customer_orders, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)
