from django.urls import path
from . import views

urlpatterns = [
    path('employee', views.Employees.as_view()),
    path('customer-name', views.OrderByCustomer.as_view()),
]
