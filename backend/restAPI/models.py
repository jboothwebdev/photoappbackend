from django.db import models

# Create your models here.
from django.db.models import CASCADE


class Customer(models.Model):
    Name = models.CharField(max_length=100, blank=False)
    Email = models.CharField(max_length=100, blank=False, unique=True)
    PhoneNumber = models.CharField(max_length=50, blank=False)
    Studio = models.CharField(max_length=50, blank=False)

class Employee(models.Model):
    Username = models.CharField(max_length=50, blank=False)
    Password = models.CharField(max_length=200, blank=False)


class Item(models.Model):
    Name = models.CharField(max_length=100, blank=False)
    Quantity = models.IntegerField(blank=False)
    Medium = models.CharField(max_length=25, blank=False)
    CustomerEmail = models.CharField(max_length=100, blank=False)
