# Generated by Django 4.0.2 on 2022-03-20 22:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restAPI', '0004_employee'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='Customer',
        ),
        migrations.AddField(
            model_name='item',
            name='CustomerEmail',
            field=models.CharField(default='empty', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customer',
            name='Email',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
