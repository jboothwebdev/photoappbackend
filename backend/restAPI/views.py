import array
from django.shortcuts import render
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404, response, HttpResponseRedirect
from django.http.response import JsonResponse
from .models import *
from .serializers import *


# Create your views here.
class Customers(APIView):

    def get(self, request):
        customers = Customer.objects.all()
        serializer = CustomerSerializer(customers, many= True)
        return Response(serializer.data)

    def get_by_name(self, Customer_name):
        try:
            return Customer.objects.get(Customer_name=Customer_name)
        except Customer.DoesNotExist:
            raise Http404

    def post(self, request):
        serializer = CustomerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class CustomersByName(APIView):

    def get_customers(self, customers_name, customers_email):
        print(customers_name)
        customers = Customer.objects.filter(Name=customers_name, Email=customers_email)
        serialized = CustomerSerializer(customers, many=True)
        print(serialized.data)
        return serialized.data


    def get(self, request):
        customer = self.get_customers(request.data.get("Name"), request.data.get("Email"))
        return Response(customer)

    def delete(self, request):
        Customer.objects.filter(Name=request.data.get('Name'), Email=request.data.get('Email')).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DeleteAll(APIView):

    def delete(self, request):
        Customer.objects.all().delete()
        Item.objects.all().delete()
        return Response("Deleted", status = status.HTTP_200_OK)

class Items(APIView):

    def get(self, request):
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ItemSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        print(serializer.data)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request): 
        Item.objects.filter(id= request.data.get('id')).delete()
        return Response("Deleted", status=status.HTTP_202_ACCEPTED)

class ItemsByCustomer(APIView):

    def post(self, request):
        customer_email: str = request.data.get('Email')
        try:
            customer = Customer.objects.filter(Email=customer_email)
            items = Item.objects.filter(CustomerEmail=customer_email)
            items_serializer = ItemSerializer(items, many=True).data
            serializers: array = []
            for item in items_serializer:
                serializers.append(item)
            return Response(serializers, status=status.HTTP_201_CREATED)
        except Item.DoesNotExist:
            raise Http404


class ItemsById(APIView):

    def get_item(self, id): 
        try:  
            return Item.objects.get(id=id)
        except Item.DoesNotExist:
            raise Http404

    def get(self, request, id):
        item = self.get_item(id)
        serializers = ItemSerializer(item ).data
        return Response(serializers, status=status.HTTP_200_OK) 

    def put(self, request, id):
        try: 
            item = self.get_item(id)
            serializer = ItemSerializer(item, data=request.data, partial=True )
            print(serializer)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            print(serializer.is_valid())
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
        except:
            raise Http404
        

class Login(APIView):

    def get_users(self):
        users = list(Employee.objects.all().values())
        return users

    def post(self, request):
        users = self.get_users()

        for user in users:
            if request.data['Username'] == user['Username'] and request.data['Password'] == user['Password']:
                return Response(user)
        return Response('none')
