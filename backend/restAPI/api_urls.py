from django.urls import path
from . import views

urlpatterns = [
    path('customers/', views.Customers.as_view()),
    path('items/', views.Items.as_view()),
    path('items/<int:id>', views.ItemsById.as_view()),
    path('customers-items', views.ItemsByCustomer.as_view()),
    path('customer-id', views.CustomersByName.as_view()),
    path('login', views.Login.as_view()),
    path('delete-all', views.DeleteAll.as_view())
]
